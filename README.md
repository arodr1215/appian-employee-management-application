# Appian Employee Management Application

**Project Description**

The Employee Management Application is designed through Appian in order to maintain and update information about employees working in an organization. With Appian's development platform, it streamlines development compared to the conventional means. It does so with the use of various Appian components in cohesion to result in a functioning business application. 

**Technologies Used**


- Appian platform with SAIL library of functions
- SQL
- Java-based functions

**Features**

- Ability to store an employee's main information, secondary info, and address in a data store entity by means of a business process model. 
- View a list of all of the different entries inside of the database, either through an editable grid, or through a report. 
- Utilize records and sites for front end use of hypothetical employees to view this information. 
- Organized the files based on best practice, as well as created security groups for each user. 

**To-do List**

- Fix minor issues in the records
- Implement Web API integration. 
